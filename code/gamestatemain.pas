unit GameStateMain;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse;

type
  TStateMain = class(TUIState)
  private
    procedure ClickNewGame(Sender: TObject);
    //LabelFps: TCastleLabel;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateMain: TStateMain;

implementation

uses
  SysUtils,
  CastleSoundEngine,
  GameStateGame;

procedure TStateMain.ClickNewGame(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  TUiState.Current := StateGame;
end;

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gamestatemain.castle-user-interface';
end;

procedure TStateMain.Start;
begin
  inherited;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('music_menu');
  (DesignedComponent('ButtonNewGame') as TCastleButton).OnClick  := @ClickNewGame;
end;

end.
