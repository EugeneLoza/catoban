unit GameStateGame;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse;

type
  TStateGame = class(TUIState)
  private
    //LabelFps: TCastleLabel;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  StateGame: TStateGame;

implementation

uses
  SysUtils,
  CastleSoundEngine;

constructor TStateGame.Create(AOwner: TComponent);
begin
  inherited;
  DesignPreload := true;
  DesignUrl := 'castle-data:/gamestategame.castle-user-interface';
end;

procedure TStateGame.Start;
begin
  inherited;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('music_game');

  //LabelFps := DesignedComponent('LabelFps') as TCastleLabel;
end;

procedure TStateGame.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  //LabelFps.Caption := 'FPS: ' + Container.Fps.ToString;
end;

function TStateGame.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then Exit; // allow the ancestor to handle keys
end;

end.
