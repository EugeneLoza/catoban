unit GameInitialize;

interface

implementation

uses SysUtils,
  CastleWindow, CastleLog, CastleUIState, CastleConfig, CastleSoundEngine,
  CastleApplicationProperties
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameStateMain
  , GameStateGame
  {$endregion 'Castle Initialization Uses'};

var
  Window: TCastleWindow;

{ One-time initialization of resources. }
procedure ApplicationInitialize;
begin
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');
  UserConfig.Load;
  SoundEngine.RepositoryURL := 'castle-data:/audio/index.xml';

  {$region 'Castle State Creation'}
  // The content here may be automatically updated by CGE editor.
  StateMain := TStateMain.Create(Application);
  StateGame := TStateGame.Create(Application);
  {$endregion 'Castle State Creation'}

  TUIState.Current := StateMain;
end;

initialization
  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2022-2022 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  Application.OnInitialize := @ApplicationInitialize;

  { Create and assign Application.MainWindow. }
  Window := TCastleWindow.Create(Application);
  Window.ParseParameters; // allows to control window size / fullscreen on the command-line
  Window.Container.BackgroundEnable := false;
  Application.MainWindow := Window;
end.
